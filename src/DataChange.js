import ReplicationSlot from "./ReplicationSlot.js";
import SQSQueue from "./SQSQueue.js";

class DataChange {

  constructor(slotName, config) {
    this.slotName = slotName;
    this.replicationSlot = new ReplicationSlot(this.slotName, this.extractReplicationSlotConfig(config));
    this.sqsQueue = new SQSQueue(process.env.AWS_QUEUE_NAME);
    this.changes = [];
  };

  async fetchChanges() {
    const rows = await this.replicationSlot.fetch();

    if (!rows) {
      return 0;
    }
    const changes = rows
      .map(change => this.parse(change))

    this.changes.push(...changes);
    return changes.length;
  }

  getCurrent() {
    return this.changes.length > 0 ? this.changes[0] : null;
  }

  async queueChange(change) {
    let { lsn = null, events } = change

    if (change === null) {
      throw Error('No change records')
    }
    lsn = this.validateLsn(lsn);

    events.length && await this.sqsQueue.send(change);
    return this.clearCurrent(lsn);
  }

  validateLsn(lsn) {
    if (lsn && lsn !== this.changes[0].lsn) {
      throw Error('LSN mismatch');
    }
    else if (!lsn) {
     return this.changes[0].lsn;
    }
  }

  /**
   * Mark the LSN that is at the top of the queue as done. When LSN is provided,
   * it must match the LSN that is the current record. Providing the LSN ensures
   * that the record being cleared is the one you are expecting.
   * @param lsn The LSN of the current change record or null.
   * @returns {Promise<void>}
   */
  async clearCurrent(lsn = null) {
    if (this.getCurrent() === null) {
      throw Error('No change records')
    }
    lsn = this.validateLsn(lsn);
    await this.replicationSlot.flush(lsn);
    return this.changes.shift();
  }

  getMatchingColumnValues(keyColumn, change) {
    if (change.kind!=='delete') {
      const columnNumber = change["columnnames"].indexOf(keyColumn);
      return change["columnvalues"][columnNumber];
    } else {
      let oldKeys = change['oldkeys'];
      const columnNumber = oldKeys["keynames"].indexOf(keyColumn);
      return oldKeys["keyvalues"][columnNumber];
    }
  }

  extractReplicationSlotConfig(config) {
    return config.reduce((res, current) => {

      const changeConfigs = current.config.changes.map(changeConfig => {
        changeConfig.job_name = current.job_name;
        return changeConfig;
      });

      return [...res, ...changeConfigs];
    }, [])
  }

  formatChange(dataChange, changeDefinition) {
    const { kind, schema, table } = dataChange;
    const dataChanges = { kind, schema, table };
    // If the list of table definitions does not include this table, it still
    // needs to be returned with the lsn to clear in the database but the data
    // do not matter because the change doesn't need to be queue'd anyway.
    const { job_name, event, columns = [] } = changeDefinition;
    dataChanges.data = {};
    columns.forEach(column => dataChanges.data[column] = this.getMatchingColumnValues(column, dataChange));
    return {
      job_name,
      event,
      ...dataChanges
    };
  };

  parse(walLogRecord) {
    const dataChanges = [];
    JSON.parse(walLogRecord.data).change.forEach(change => {
      this.replicationSlot.configuration.forEach(config => {
        if (config.table === change.table && config.schema === change.schema && config.changes.includes(change.kind)) {
          let filter = false;
          // check for filter obj
          if(config.filter) {
            // iterate over filter obj check each reg against the corresp. data value
            for (const property in config.filter) {
              const r = new RegExp(config.filter[property]);
              let data = this.getMatchingColumnValues(property, change);
              // flag as excluding if found
              if(!r.test(data)) {
                filter = true;
              }
            }
          }
          // only push if we didn't find a false test
          if(!filter) {
            dataChanges.push(this.formatChange(change, config))
          }
        }
      })
    })

    return {
      lsn: walLogRecord.lsn,
      events: dataChanges
    };
  }
}

export default DataChange;

import * as db from 'pdc-common/wapdc-db'

class ReplicationSlot {


  constructor(slotName, configuration) {
    this.slotName = slotName;
    this.configuration = configuration;
    this.tableList = this.getSchemaQualifiedTables().join(", ");
  }

  getSlotName() {
    return this.slotName;
  }

  getSchemaQualifiedTables() {
    return this.configuration.map((tableDefinition) => {
      return `${tableDefinition["schema"]}.${tableDefinition["table"]}`;
    })
  }

  /**
   * Marks logical changes as consumed so that they are cleared in the database
   * log and will not be returned by subsequent calls to fetch.
   * @param lsn Point to flush or null to flush all.
   * @returns {Promise<void>}
   */
  async flush(lsn) {
    lsn = lsn ? `'${lsn}'` : "null";

    /* filtered to a table that doesn't exist and just count(1) so that the data
    transfer over the network is minimized because we don't actually need the
    changes.
     */
    const q = `select count(1) from pg_logical_slot_get_changes(
      '${this.slotName}', ${lsn}, null, 'include-xids', '0',
      'add-tables', 'public.xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
      where exists (select * from pg_replication_slots where slot_name = '${this.slotName}');`;

    await db.execute(q);
  }

  async fetch() {
    const q = `select * from pg_logical_slot_peek_changes('${this.slotName}', null, null,
    'include-xids', '0',
    'add-tables', '${this.tableList}')
    where exists (select * from pg_replication_slots where slot_name = '${this.slotName}') and cast(data as json)->'change'->0 is not null
    -- if the maximum lsn is not from a table we are looking at, we should grab that record too
    union (select * from (select * from pg_logical_slot_peek_changes('${this.slotName}', null, null,
    'include-xids', '0', 'add-tables', '${this.tableList}') where exists (select * from pg_replication_slots where slot_name = '${this.slotName}')
    order by 1 desc limit 1) s where cast(data as json)->'change'->0 is null)
    order by 1;
    `;

    let response = {}
    try {
      response = await db.query(q);
    } catch (err) {
      log.error(`Error querying replication slot: `, err)
      throw Error(err)
    }
    return response
  }

}

export default ReplicationSlot;

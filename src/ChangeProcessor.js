

class ChangeProcessor {


  constructor(rds, socrata) {
    this.db = rds;
    this.socrata = socrata
    this.tasks = {};
  }

  getEventFromConfiguration(settings, job_name, event_name) {
    let job = settings.find(job  => job.job_name === job_name);
    if (job) {
      if (job.config && job.config.events && job.config.events[event_name]) {
        return job.config.events[event_name];
      }
    }
    return null;
  }

  async processUpdate(setting, data) {
    let parameters = [];
    if (setting.criteria) {
      parameters = setting.criteria.map(field => data[field]);
    }
    let rows = await this.db.loadRecords(setting.source, parameters);
    if(rows && rows.length) {
      return rows;
    } else {
      return [];
    }
  }

  async processReplace(setting, data) {
    let parameters = [];
    if (setting.criteria) {
      parameters = setting.criteria.map(field => data[field]);
    }

    const { id = 'id' } = setting;

    let dbRows = await this.db.loadRecords(setting.source, parameters);
    let odRows = await this.socrata.loadRecords(setting.dataset_id, setting.destination, parameters);

    const ids = dbRows.map(row => row[id].toString());
    const deleteRows = odRows.filter(row => !ids.includes(row[id].toString()))
      .map(row => ({ [id]: row[id], ':deleted': true}));

    const rows = [...dbRows, ...deleteRows];

    if(rows.length) {
      return rows;
    } else {
      return [];
    }
  }

  async processDelete(setting, data) {
    let parameters = [];
    if (setting.criteria) {
      parameters = setting.criteria.map(field => data[field]);
    }
    let rows = await this.socrata.loadRecords(setting.dataset_id, setting.source, parameters);
    if (rows && rows.length) {
      return rows.map(row => {row[':deleted'] = true; return row});
    } else {
      return [];
    }
  }

  async processChange(queueItem = null) {
    let settings =  await this.db.getConfiguration();
    let messageContent = JSON.parse(queueItem.body);
    messageContent.events.forEach(event => {
      let setting = this.getEventFromConfiguration(settings, event.job_name, event.event);

      if (setting) {
        switch (setting.type) {
          case 'upsert':
            this.appendTasks(setting.dataset_id, this.processUpdate(setting, event.data));
            break;
          case 'replace':
            this.appendTasks(setting.dataset_id, this.processReplace(setting, event.data));
            break;
          case 'delete':
            this.appendTasks(setting.dataset_id, this.processDelete(setting, event.data));
            break;
        }
      }
    });
    return true;
  }

  async flushRows() {
    const responses = Object.entries(this.tasks).map(async ([dataset_id]) => {
      const resolved = (await Promise.all(this.tasks[dataset_id])).flat();
      return this.socrata.upsertRecords(dataset_id, resolved);
    })
    return Promise.all(responses);
  }

  appendTasks(dsid, rows) {
    if (this.tasks[dsid]) {
      this.tasks[dsid].push(rows);
    } else {
      this.tasks[dsid] = [rows]
    }
  }
}

export default ChangeProcessor;

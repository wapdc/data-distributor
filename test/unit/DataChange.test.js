
import env from 'dotenv';
env.config()
import ReplicationSlot from "../../src/ReplicationSlot";
import DataChange from "../../src/DataChange";
import SQSQueue from "../../src/SQSQueue";
import {jest} from '@jest/globals';
let data1, data2, data3, data4, data5, config;
const slot = process.env.TEST_SLOT;

const mockFetch = jest.fn();
ReplicationSlot.prototype.fetch = mockFetch;

const mockFlush = jest.fn();
ReplicationSlot.prototype.flush = mockFlush;

const mockSend = jest.fn();
SQSQueue.prototype.send = mockSend;

beforeEach(async () => {
  jest.resetAllMocks();

  data1 =
    [
      {
        "lsn": "10E/74064D30",
        "xid": "19765",
        "data": "{\"change\":[{\"kind\":\"insert\",\"schema\":\"public\",\"table\":\"data_distributor_test_table\",\"columnnames\":[\"key_column\",\"data_column\"],\"columntypes\":[\"integer\",\"text\"],\"columnvalues\":[0,\"0\"]}]}"
      },
      {
        "lsn": "10E/74079668",
        "xid": "19766",
        "data": "{\"change\":[{\"kind\":\"insert\",\"schema\":\"public\",\"table\":\"data_distributor_test_table\",\"columnnames\":[\"key_column\",\"data_column\"],\"columntypes\":[\"integer\",\"text\"],\"columnvalues\":[1,\"a\"]}]}"
      },
      {
        "lsn": "10E/74079718",
        "xid": "19767",
        "data": "{\"change\":[{\"kind\":\"insert\",\"schema\":\"public\",\"table\":\"data_distributor_test_table\",\"columnnames\":[\"key_column\",\"data_column\"],\"columntypes\":[\"integer\",\"text\"],\"columnvalues\":[2,\"b\"]}]}"
      },
      {
        "lsn": "10E/740797C8",
        "xid": "19768",
        "data": "{\"change\":[{\"kind\":\"insert\",\"schema\":\"public\",\"table\":\"data_distributor_test_table\",\"columnnames\":[\"key_column\",\"data_column\"],\"columntypes\":[\"integer\",\"text\"],\"columnvalues\":[3,\"c\"]}]}"
      },
      {
        "lsn": "10E/740798F8",
        "xid": "19769",
        "data": "{\"change\":[{\"kind\":\"insert\",\"schema\":\"public\",\"table\":\"data_distributor_test_table\",\"columnnames\":[\"key_column\",\"data_column\"],\"columntypes\":[\"integer\",\"text\"],\"columnvalues\":[4,\"d\"]},{\"kind\":\"insert\",\"schema\":\"public\",\"table\":\"data_distributor_test_table\",\"columnnames\":[\"key_column\",\"data_column\"],\"columntypes\":[\"integer\",\"text\"],\"columnvalues\":[5,\"e\"]}]}"
      }
    ];
  data2 =
    [
      {
        "lsn": "10E/74089668",
        "xid": "19966",
        "data": "{\"change\":[{\"kind\":\"insert\",\"schema\":\"public\",\"table\":\"data_distributor_test_table\",\"columnnames\":[\"key_column\",\"data_column\"],\"columntypes\":[\"integer\",\"text\"],\"columnvalues\":[12,\"l\"]}]}"
      }
    ]
  data3 = [];

  data4 = [
      {
        "lsn": "153/B80892F8",
        "xid": "25160",
        "data": "{\"change\":[{\"kind\":\"insert\",\"schema\":\"public\",\"table\":\"data_distributor_test_table\",\"columnnames\":[\"key_column\",\"data_column\"],\"columntypes\":[\"integer\",\"text\"],\"columnvalues\":[1,\"a\"]}]}"
      },
      {
        "lsn": "153/B8089370",
        "xid": "25161",
        "data": "{\"change\":[{\"kind\":\"update\",\"schema\":\"public\",\"table\":\"data_distributor_test_table\",\"columnnames\":[\"key_column\",\"data_column\"],\"columntypes\":[\"integer\",\"text\"],\"columnvalues\":[1,\"b\"],\"oldkeys\":{\"keynames\":[\"key_column\"],\"keytypes\":[\"integer\"],\"keyvalues\":[1]}}]}"
      },
      {
        "lsn": "153/B8089420",
        "xid": "25162",
        "data": "{\"change\":[{\"kind\":\"insert\",\"schema\":\"public\",\"table\":\"data_distributor_test_table\",\"columnnames\":[\"key_column\",\"data_column\"],\"columntypes\":[\"integer\",\"text\"],\"columnvalues\":[3,\"c\"]}]}"
      },
      {
        "lsn": "153/B8089490",
        "xid": "25163",
        "data": "{\"change\":[{\"kind\":\"delete\",\"schema\":\"public\",\"table\":\"data_distributor_test_table\",\"oldkeys\":{\"keynames\":[\"key_column\"],\"keytypes\":[\"integer\"],\"keyvalues\":[3]}}]}"
      }
    ];
  data5 = [
      {
        "lsn": "10E/74089515",
        "xid": "25190",
        "data": "{\"change\":[]}"
      }
  ];
  config = [
    {
      job_name: 'test_job',
      config: {
        "changes": [
          {
            "schema" : "public",
            "table": "data_distributor_test_table",
            "changes": ["insert", "update"],
            "columns": ["key_column"],
            "event": "updateDistributorTestTable"
          },
          {
            "schema" : "public",
            "table": "data_distributor_test_table",
            "changes": ["delete"],
            "columns": ["data_column"],
            "event": "deleteFromDataDistributorTable"
          },
          {
            "schema" : "public",
            "table": "fa_statement",
            "changes": ["delete"],
            "columns": ["statement_id"],
            "event": "deleteStatement"
          }
        ]
      },
      is_enabled: true,
    },
    {
      job_name: 'another_test_job',
      config: {
        "changes": [
          {
            "schema" : "public",
            "table": "data_distributor_test_table",
            "changes": ["insert", "update"],
            "columns": ["data_column"],
            "event": "updateFromDataDistributorTable",
            "filter": {
              "data_column": "0"
            }
          },
          {
            "schema" : "public",
            "table": "data_distributor_test_table",
            "changes": ["insert", "update"],
            "columns": ["data_column"],
            "event": "eventThatShouldNotFire",
            "filter": {
              "data_column": "zzzz"
            }
          },
          {
            "schema" : "public",
            "table": "fa_statement",
            "changes": ["insert", "update"],
            "columns": ["statement_id"],
            "event": "updateStatement"
          }
        ]
      },
      is_enabled: true,
    }
  ];
  mockFetch.mockImplementationOnce(() => {
    return Promise.resolve(data1);
  });
  mockFetch.mockImplementationOnce(() => {
    return Promise.resolve(data2);
  });
  mockFetch.mockImplementation(() => {
    return Promise.resolve(data3);
  });
  mockFetch.mockImplementationOnce(() => {
    return Promise.resolve(data4)
  });
  mockFetch.mockImplementationOnce(() => {
    return Promise.resolve(data5)
  });
  mockFlush.mockImplementation((lsn) => {
    let index = data1.findIndex((element) => {
      return element.lsn === lsn
    });
    while (index >= 0) {
      data1.shift();
      index--;
    }
    Promise.resolve(undefined)
  });
});

describe('fetch and clear changes', () => {
  test('should have expected initial rowcount', async () => {
    const dataChange = new DataChange(slot, config);
    const changes = await dataChange.fetchChanges();
    expect(changes).toEqual(data1.length);
  });

  test('should get first expected lsn', async () => {
    const firstLSN = data1[0].lsn;
    const dataChange = new DataChange(slot, config);
    await dataChange.fetchChanges();
    expect(dataChange.getCurrent().lsn).toEqual(firstLSN);
  });

  test ('should have expected output', async() => {
    const dataChange = new DataChange(slot, config);
    await dataChange.fetchChanges();
    const events = dataChange.getCurrent().events;
    const [ firstChange, secondChange ] = events;
    expect(events.length).toBe(2);
    const firstExpected = {'job_name': 'test_job', 'event': 'updateDistributorTestTable', 'kind': 'insert', 'schema': 'public', 'table': 'data_distributor_test_table', 'data' : {'key_column' : 0}};
    const secondExpected = {'job_name': 'another_test_job', 'event': 'updateFromDataDistributorTable', 'kind': 'insert', 'schema': 'public', 'table': 'data_distributor_test_table', 'data' : {'data_column' : "0"}};
    expect(JSON.stringify(firstChange)).toEqual(JSON.stringify(firstExpected));
    expect(JSON.stringify(secondChange)).toEqual(JSON.stringify(secondExpected));
  });

  test('flush without lsn should produce next LSN', async () => {
    const secondLSN = data1[1].lsn;
    const dataChange = new DataChange(slot, config);
    await dataChange.fetchChanges();
    await dataChange.clearCurrent();
    expect(dataChange.getCurrent().lsn).toEqual(secondLSN);
  });

  test('flush with lsn should produce next LSN', async () => {
    const firstLSN = data1[0].lsn;
    const secondLSN = data1[1].lsn;
    const dataChange = new DataChange(slot, config);
    await dataChange.fetchChanges();
    await dataChange.clearCurrent(firstLSN);
    expect(dataChange.getCurrent().lsn).toEqual(secondLSN);
  });

  test('flush with invalid lsn should throw', async () => {
    expect.assertions(1);
    const dataChange = new DataChange(slot, config);
    await dataChange.fetchChanges();

    await expect(dataChange.clearCurrent('--------')).rejects.toEqual(
        Error('LSN mismatch')
    );
  });

  test('flush with valid but not first lsn should throw', async () => {
    expect.assertions(1);
    const secondLSN = data1[1].lsn;
    const dataChange = new DataChange(slot, config);
    await dataChange.fetchChanges();

    await expect(dataChange.clearCurrent(secondLSN)).rejects.toEqual(
        Error('LSN mismatch')
    );
  });

  test('flush all should return empty', async () => {
    expect.assertions(1);
    const dataChange = new DataChange(slot, config);
    const changes = await dataChange.fetchChanges();
    for (let i = 0; i < changes; i++) {
      await dataChange.clearCurrent();
    }
    expect(dataChange.getCurrent()).toBeNull();
  });

  test('fetch after flush all should return new rows', async () => {
    expect.assertions(1);
    const dataChange = new DataChange(slot, config);
    const changes = await dataChange.fetchChanges();
    for (let i = 0; i < changes; i++) {
      await dataChange.clearCurrent();
    }
    await dataChange.fetchChanges();
    const change = dataChange.getCurrent();
    expect(change.lsn).toEqual(data2[0].lsn);
  });

  test('fetch after flush some should append new rows at end', async () => {
    expect.assertions(2);
    const dataChange = new DataChange(slot, config);
    const firstFetchChangeCount = await dataChange.fetchChanges();
    // Will leave 2 rows
    for (let i = 0; i < firstFetchChangeCount-2; i++) {
      await dataChange.clearCurrent();
    }
    const originalNextLsn = dataChange.getCurrent().lsn;
    await dataChange.fetchChanges();
    const newNextLsn = dataChange.getCurrent().lsn;

    // Next up LSN should not change
    expect(originalNextLsn).toEqual(newNextLsn);

    // Clearing remaining two records from the first fetch, should get from second fetch
    await dataChange.clearCurrent();
    await dataChange.clearCurrent();
    const change = dataChange.getCurrent();
    expect(change.lsn).toEqual(data2[0].lsn);
  });
});

describe('queue changes', () => {
  beforeEach(() => {
    mockSend.mockImplementation(() => {
      return Promise.resolve(undefined);
    });
  });
  test('should queue change and flush, producing next lsn', async () => {
    expect.assertions(1);
    const thirdLSN = data1[2].lsn;
    const dataChange = new DataChange(slot, config);
    let changeCount = await dataChange.fetchChanges();
    while (changeCount--) {
      await dataChange.queueChange(dataChange.getCurrent());
      if (changeCount === 3) {
        expect(dataChange.getCurrent().lsn).toEqual(thirdLSN);
      }
    }
  });
  test('should queue all valid changes', async () => {
    const dataChange = new DataChange(slot, config);
    while(await dataChange.fetchChanges()) {
      let change;
      while((change = dataChange.getCurrent())) {
        await dataChange.queueChange(change);
      }
    }
    expect(mockSend.mock.calls.length).toBe(10);
  });
  test('should not queue empty changes', async () => {
    mockFetch.mockImplementationOnce(() => {
      return Promise.resolve([
        {
          "lsn": "10E/74389511",
          "xid": "25190",
          "data": "{\"change\":[]}"
        },
        {
          "lsn": "10E/74089577",
          "xid": "25191",
          "data": "{\"change\":[]}"
        },
      ])
    });
    mockFetch.mockImplementationOnce(() => {
      return Promise.resolve(
        [
          {
            "lsn": "153/B80662F6",
            "xid": "25160",
            "data": "{\"change\":[{\"kind\":\"insert\",\"schema\":\"public\",\"table\":\"data_distributor_test_table\",\"columnnames\":[\"key_column\",\"data_column\"],\"columntypes\":[\"integer\",\"text\"],\"columnvalues\":[1,\"a\"]}]}"
          },
        ]
      )
    });
    const dataChange = new DataChange(slot, config)
    const invalidLSNs = []
    let change;
    let processedChanges = [];
    while(change = await dataChange.fetchChanges()) {
      while((change = dataChange.getCurrent())) {
        if (!change.events.length) {
          invalidLSNs.push(change.lsn)
        }
        processedChanges.push(await dataChange.queueChange(change));
      }
    }

    const sentLSNs = mockSend.mock.calls.map(([ change ]) => change.lsn)
    const flushedLSNs = mockFlush.mock.calls.map(([ change ]) => change)
    const processedLSNs = processedChanges.map(({ lsn }) => lsn)

    expect(sentLSNs).toEqual(
      expect.not.arrayContaining(invalidLSNs)
    )
    expect(processedLSNs).toEqual(
      expect.arrayContaining(invalidLSNs)
    )
    expect(flushedLSNs).toEqual(processedLSNs);
  })
});

import {jest} from '@jest/globals'
import env from 'dotenv'
env.config();
// Because axios calls are only mocked on initial
// import of the module we create this abstraction to allow
// us to replace the implementation of axios calls with different
// functions.
const mockAxios = {
    get: jest.fn(),
    post: jest.fn()
}
jest.unstable_mockModule('axios', () => ({default: {
    get: (path, config) => mockAxios.get(path, config),
    post: (path, data) => mockAxios.post(path, data)
}}));

const {default: Socrata} = await import("../../src/Socrata.js");




describe('Socrata tests', () => {
    test('loads data set record', async () => {
        let s = new Socrata();
        await s.initialize();
        mockAxios.get.mockImplementation( () => Promise.resolve({ data: {}}));
        let value = String.raw`1'2`
        s.loadRecords('test_data_set', "select * where id=$1", {"id": value}).then(records => {
          expect(records).toBeDefined();
          expect(mockAxios.get).toBeCalled();
          expect(mockAxios.get).toHaveBeenLastCalledWith("https://data.wa.gov/resource/test_data_set.json", expect.anything());
          let params = mockAxios.get.mock.calls[0][1]["params"];
          expect(params['$query']).toBe(`select * where id='1''2'`)
        });
    });

    test('upsert failures result in error', async () => {
        let s = new Socrata();
        await s.initialize();
        expect.assertions(1);
        mockAxios.post = jest.fn().mockImplementation(() => Promise.reject(`Bad things occurred.`));
        await s.upsertRecords('test_data_set', [{}]).catch((e) => {
            expect(e).toBeDefined();
        })
    });

    test('loadRecords failures result in error', async () => {
        let s = new Socrata();
        await s.initialize();
        expect.assertions(1);
        mockAxios.get.mockImplementation(() => Promise.reject(`Bad things occurred.`))
        await s.loadRecords('test_data_set', [{}], 123).catch((e) => {
            expect(e).toBeDefined();
        })
    });
})

# Open Data publication strategy

To understand the design constraints for this system it is a good iea to review the [Publication Use Cases](useCases.md)
to get a better feeling for the complexity of the real time updates required.

## Real Time Updates

Database writes to specific tables are be monitored using [Postgres Replication](https://www.postgresql.org/docs/10/runtime-config-replication.html).

- An AWS lambda (DataChange) runs once every two minutes and reads from the postgres replication slot.
- Each read from the replication represents all insert/update/delete events that happened on any tables.
- The DataChange function reads the configuration and fires an SQS message for every event that matches the type of change
  and table of change defined in any job.  The **changes** section of the job definition determine which data
  are passed as parameters to each event in the message.
- The SQS message will trigger a lambda which reads messages from the queue and performs updates based on the type of
  event indicated in the SQS message. Each event type corresponds to a different algorithm for updating Open data based on
  the parameters passed in the message.

## Full Refreshes

**NOTE:**  This strategy has not really been implementd yet.

Scheduled lambda functions should perform full or large data set populations once per day?  The function
should read a log of what data sets jobs need to be refreshed at what frequency, when the last time that
was run, and queue up the appropriate SQS messages to perform the updates.

## Fanning out messages

Because an update to a table could cause a cascade of open data updates, when the DataChange lambda
processes a message containing a change, it will need to be able to perform several configured updates.
The transaction should only be considered successful if **all** of the open data updates happen for that
message.

## Open Data Publication

Open data are published through the execution of a lambda (Process) that would read from a configuration
table to determine whether any full datasets need to be published. It should queue SQS messages
(one per data set) to trigger the refresh of the set.

The lambda that reads the queue message needs to have a strategy of "making open data right".
This means that it should not just  "remove" a record from a data set, but should check the state
of the related records in the data in both the open data data set and the RDS and:

- create the record in the data set if it should exist
- remove the record in the data set if it should not exist.

This is important so that the order of the SQS messages will not matter, since we are reaching
for parallelized execution.

## Monitoring
Monitoring for various modes of failure has been configured via CloudWatch. 

Addressing potential modes of failures:
- Replication Slot fills up (exceeds thresholds for unacceptable period of time)
  - Handled via ReplicationSlotDiskUsage alarm.
- RDS SQL error occurs while trying to process a queue message.
  - Dead letter queue monitoring will inform us of these failures.
- Socrata error occurs in upserting a record.
  - Also handled by dead letter queues (correlate timestamps with messages)
- Processing too many records (lambda TTL gets exceeded)
  - Lambda has a duration CloudWatch Metric that can warn us if we are processing transactions that near the TTL.
  - Timed out `processChange` requests will end up in the dead-letter queue after 10 retries occur.
- Configuration bug causes us to ignore all of the replication slot messages and not queue messages.
  - Anomaly-based CloudWatch alarm on messagesSent/messagesReceived metric for `queueChanges` is not useful because it's just a cron job that we can expect to fire on a timer.
   The consequences of this with regard to the replication disk usage are already being observed by another CloudWatch alarm.

In order to ensure that the Lambda functions have enough time to complete their operations, the timeout settings have been set as follows:
   - queueChanges: 4 minutes (240 seconds)
   - processChange: 15 minutes (900 seconds)

These changes were applied by modifying the `template.yaml` file in the handler definitions.
Fifteen minutes represents the maximum Time-to-Live (TTL) for a Lambda function. It cannot be increased further.
The timeouts are defined in seconds.
`queueChanges` has a light buffer of 10 seconds below two minutes to allow the preceding lambda invocation to complete before proceeding with subsequent invocations.

### Provisioned Concurrency
The reserved concurrency of the `queueChanges` lambda has been set to 1. This is important because we do not want more than one lambda reading and flushing the replication slot at any given time.
Reserved concurrency for the `processChange` lambda has been capped at 50. This number represents a reasonable, but arbitrary, figure that comes in at about 15% of the maximum number of connections the postgres RDS instance will accept.

> Importantly, the number of retries on each primary queue (live and demo) have been set to three. 
> **Any increase in the number of retries translates into a geometric expansion in the number of connections made to the 
> PostgreSQL RDS instance**. 
> Initial attempts at allowing 10 retries resulted in a depletion of RDS instance hardware resources.

## Diagram

![Data Distributor Diagram](diag.png)

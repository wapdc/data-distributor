export const batchConfig = [
  {
    job_name: 'expenditures_by_candidates_and_political_committees',
    config: {
      "events": {
        "newExpenditureData": {
          "type": "upsert",
          "dataset_id": "dsid-1234",
          "source": "select * from od_expenditures where report_number=$1",
          "criteria": ["report_id"]
        },
        "deleteExpenditureData": {
          "type": "delete",
          "dataset_id": "dsid-1234",
          "source": "select id where report_number=$1",
          "criteria": ["report_id"]
        },
        "updateExpenditureData": {
          "type": "upsert",
          "dataset_id": "dsid-1234",
          "source": "select * from od_expenditures where id=$1",
          "criteria": ["transaction_id"]
        },
        "replaceExpenditureData": {
          "type": "replace",
          "dataset_id": "dsid-1234",
          "source": "select * from od_expenditures where report_number=$1",
          "destination": "select id where report_number=$1",
          "criteria": ["report_id"]
        }
      }
    },
    is_enabled: true,
  }
];

export const batchRDSResponse2 = [
  {
    "id": "761520227",
    "report_number": "100491285",
    "origin": "A/GT50",
    "filer_id": "SNOHRC 206",
    "amount": "444.00",
    "expenditure_data": "2012-05-25T00:00:00"
  }
];
export const batchRDSResponse1 = [
  {
    "id": "761520222",
    "report_number": "100490398",
    "origin": "A/LE50",
    "filer_id": "NASSC2 110",
    "amount": "11.00",
    "expenditure_data": "2012-07-31T00:00:00"
  },
  {
    "id": "761520223",
    "report_number": "100490398",
    "origin": "A/LE50",
    "filer_id": "NASSC2 110",
    "amount": "217.00",
    "expenditure_data": "2012-07-31T00:00:00"
  },
];

export const batchMessage = {
  "MessageId": "864f09a0-603d-404f-bcb2-942b8674b2b2",
  "ReceiptHandle": "AQEBzUNDv9pcAG047Gc8zVII9X8d9/WkaQvLSQ3Hj6Uf6nmoOi7U50BO4ielu0OWt/wAuIaxt/v/9et6yA7Scrdf1xskXGhky6DUwchLr/FUKaoSM5V9jDUXfYYL90MGdDJVfKR6zFC41EzzbvE0h1aKSrqCib2D6zaaJSKCrtPXiE8dlmMi+jN8r+JXccSZKB5czu48ow/HLkCnHUKli6LH/Nu1V2GqHUq1DLXirbX2BocQ4VaFiZ+5YJb2hkNrVcaLjxZu6hFZlzm9UPe3KfzWkGwskBpiNms7ofgfkdGC/Ny3WAJuJAfFV5uxDvFHYgUJEarppbSZcuWNHiTw1YvlqlSUzD6TIA+Q7NcHvnBkfnSv6zqJP3iDqJ4hOoH0mFMIB5Teyzj8twHrAkbMju4K2A==",
  "MD5OfBody": "e520303d4199ebafe1df97c4cc716cba",
  "body": '{"lsn": "10E/74064D39","events": [{"job_name": "expenditures_by_candidates_and_political_committees","event": "replaceExpenditureData", "data": {"report_id": "100490398"}},{"job_name": "expenditures_by_candidates_and_political_committees","event": "updateExpenditureData", "data": {"report_id": "100491285"}}]}'
};

export const batchRequest = [
  {
    "id": "761520222",
    "report_number": "100490398",
    "origin": "A/LE50",
    "filer_id": "NASSC2 110",
    "amount": "11.00",
    "expenditure_data": "2012-07-31T00:00:00"
  },
  {
    "id": "761520223",
    "report_number": "100490398",
    "origin": "A/LE50",
    "filer_id": "NASSC2 110",
    "amount": "217.00",
    "expenditure_data": "2012-07-31T00:00:00"
  },
  {
    "id": "761520224",
    ":deleted": true
  },
  {
    "id": "761520227",
    "report_number": "100491285",
    "origin": "A/GT50",
    "filer_id": "SNOHRC 206",
    "amount": "444.00",
    "expenditure_data": "2012-05-25T00:00:00"
  }
]

export const batchResponse = {
  "rows_created": 1,
  "Errors": 0,
  "rows_updated": 2,
  "Rows Created": 1,
  "Rows Updated": 2,
  "rows_deleted": 1,
  "Rows Deleted": 1
};

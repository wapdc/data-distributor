# Development

### References

* [AWS SAM](https://docs.aws.amazon.com/serverless-application-model/)
* [AWS CLI](https://aws.amazon.com/cli/)
* [lambda-log](https://www.npmjs.com/package/lambda-log?activeTab=readme)
* [wal2json Package](https://www.npmjs.com/package/node-wal2json)
* [PG Logical Replication](https://github.com/kibae/pg-logical-replication/blob/master/index.js)
* [AWS Delete EventSourceMapping](https://docs.aws.amazon.com/cli/latest/reference/lambda/delete-event-source-mapping.html)


## Installation

Checkout the projet and using node 18 run the following command.
```sh
npm install
```

The AWS CLI must be used to supply the AWS_REGION, AWS_ACCESS_KEY_ID, and AWS_SECRET_ACCESS_KEY VARIABLES to the aws sdk 
 when executing commands or unit tests. 

1. Install the CLI (link above).
2. Configure the AWS CLI by running `aws configure` and supplying the required credentials.

You also should make sure you have installed the aws sam command line tools using the link above. 

## Workflow

Most code changes can be accomplished by making appropriate changes to either unit tests or integeration tests. 

## Running unit tests

Unit tests will connect to the database, but do not actually require a fully functioning AWS environment.  Assuming you 
have a core database up and running you should be able to create a normal just run configuration that runs all tests 
in the directory "tests/unit".

## Running Integration Tests

Because local docker postgres images do not support full replication, integration tests can must be done pointing at the 
demo environment.  In order to achieve this you need to create a run configuration that connects to the demo environment 
without using the proxy.  Step through debugging of integration tests can be achieved using php storm run configuration 
as follows: 

- Create a JEST unit test run configuration the runs all the tests in the tests/integeration folder
- Alter the environment variables for the fun configuration as indicated below

```shell
STAGE=demo
PGHOST=<hostname for RDS Demo instance>
```

You should now be able to run and interactively debug the integration tests using this run configuration 

## Deploying debug code to demo 

In some situation you may need to create debug versions of the product to deploy to the demo aws infrustructure. After 
making sure your local changes run with unit tests, be sure deploy the application with the following commands. 

```shell
sam build
sam deploy 
```
 
### Testing the data distributor

As mentioned earlier, the change processing mechanism requires replication which is not currently available in the lando 
environments.  In order to test the queue changes processor you need to have a table that is monitored for changes with a 
configuration that is enabled in demo. Most datatables configurations are disabled in demo. 

Basic steps to test the queue changes function are: 

- Optionally clear out any data that exists in the **PDC Test Data** dataset by replacing the dataset with the contents  
of [sample/test-data-set.csv](../sample/test-data-set.csv) on socrata so that you have a known starting point.
- execute the sql found in [sample/pdc_test_dataset.sql](../sample/pdc_test_dataset.sql)
- manually run the queueChanges lambda from the aws console.

The script above performs the following tasks in demo: 

- Creates a table (pdc_test_dataset) and view (od_pdc_test_dataset) for publishing.
- Creates the replication slot that is monitored by the queueChanges lambda.
- Inserts the sample config represented in [sample/pdc_test_dataset.json](../sample/pdc_test_dataset_config.json) 
- Performs an insert on the pdc_test_dataset which require an update. 

Inserts to the data should be found in the the PDC Test Data dataset after the lambdas run successfully. 

If you need to do extensive testing you can enable the schedule for the queueChanges lambda 
so that it runs periodically.  **Remember to disable the event after testing is complete**

As needed, perform inserts or updates on the table pdc_test_dataset and run the queueChanges lambda to perform the tests.  
The script mentioned above contains an example insert statement for modifying this table.  After any update/insert or 
delete is performed, the changes should be reflected in the PDC Test Data dataset. 

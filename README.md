# Data Distributor Documentation

This project facilitates the publication of data for the Washington Public Disclosure commission from data housed in the
agency's Postgres database the data warehouse.

- [Design Strategy](docs/strategy.md)
- [Developer Documentation](docs/development.md)
- [Configuration](docs/configuration.md)
- [Infrastructure](docs/infrastructure.md)
- [Diagnostics and Logging](docs/diagnostics.md)
- [Talking to Socrata with cURL](docs/curlExamples.md)

@TODO: Adding new data sets






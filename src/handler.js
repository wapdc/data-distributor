import log from 'lambda-log'

import DataChange from "./DataChange.js"
import ChangeProcessor from './ChangeProcessor.js'
import * as db from 'pdc-common/wapdc-db'
import dbProcessor from "./DBProcessor.js";
import Socrata from "./Socrata.js";

export const processQueueItem = async function (event) {
  log.info(`*** Executing processChange handler ***`)
  let results = [];
  await db.connect();

  try {
    const socrata = new Socrata();
    await socrata.initialize();
    const changeProcessor = new ChangeProcessor(dbProcessor, socrata);
    const tasks = []
    event.Records.forEach((record) => {
      tasks.push(changeProcessor.processChange(record))
    });
    await Promise.all(tasks);
    results = await changeProcessor.flushRows();
  }
  finally {
    await db.close();
  }


  let responseBody = {message: "changes processed", results};
  log.info(`Complete. Response body: `, responseBody);

  return {
    statusCode: 200,
    body: JSON.stringify(responseBody),
  };
};

export const queueChanges = async function () {
  log.info(`*** Executing queueChanges handler ***`)
  await db.connect();

  const {  pg_current_wal_lsn  } = await db.fetch('select * from pg_current_wal_lsn()')

  const slotExists = (await db.fetch(
    `select 1 from pg_replication_slots where slot_name = '${process.env.SLOT_NAME}'`
  ));

  if (!slotExists) {
    log.info(`Creating replication slot...`)
    await db.execute(
      `select pg_create_logical_replication_slot('${process.env.SLOT_NAME}', 'wal2json')`
    );
  }

  let configuration = await dbProcessor.getConfiguration();

  const dataChange = new DataChange(process.env.SLOT_NAME, configuration);
  const queuedChanges = [];
  let changesHappened = false;

  while (await dataChange.fetchChanges()) {
    let change;
    changesHappened = true;
    while ((change = dataChange.getCurrent())) {
      const { events } = await dataChange.queueChange(change);
      events.length && queuedChanges.push(events);
    }
  }

  if (!changesHappened) {
    await db.execute(`select count(1) from pg_logical_slot_get_changes(
        '${process.env.SLOT_NAME}', '${pg_current_wal_lsn}', null, 'include-xids', '0',
        'add-tables', 'public.xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
where exists (select * from pg_replication_slots where slot_name = '${process.env.SLOT_NAME}')`);
  }

  await db.close()
  log.info(`Processing complete: `, {
    changesQueued: queuedChanges.length,
    events: queuedChanges.flat()
  });

  return {
    statusCode: 200,
    body: JSON.stringify({ count: queuedChanges.length }),
  };
};

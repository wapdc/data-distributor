import ReplicationSlot from '../../src/ReplicationSlot';
import {jest, describe, expect, test, beforeAll, afterAll} from '@jest/globals'
import * as db from 'pdc-common/wapdc-db'


const config = [
  {
    "schema" : "public",
    "table": "data_distributor_test_table",
    "changes": ["insert", "update"],
    "columns": ["key_column"],
    "event": "updateStatementFromCandidacy"
  },
  {
    "schema": "public",
    "table": "data_distributor_test_table_x",
    "changes": ["insert", "update"],
    "columns": ["key_one", "key_two"],
    "event": "updateStatementFromCandidacy"
  },
  {
    "schema" : "public",
    "table": "data_distributor_test_table_y",
    "changes": ["delete"],
    "columns": ["key_one", "key_two", "key_three"],
    "event": "deleteStatement"
  }
];

beforeEach(async () => {
  jest.setTimeout(30000);
});

export async function getSetting(setting) {
  const q = `select setting from pg_settings where name = '${setting}'`;
  return await db.fetch(q).setting;
}

export async function setupReplTables(slot, tables) {
  const rows = await db.query(
      `select 1 from pg_replication_slots where slot_name = '${slot}'`
  );
  if (rows.length) {
    await db.execute(
        `select pg_drop_replication_slot('${slot}')`
    );
  }

  // create tables in parallel but wait for all to finish
  await Promise.all(tables.map(async (tableName) => {
    const tableExists = (await db.fetch(
        `select table_name from information_schema.tables where table_name = '${tableName}'`
    ));
    if (tableExists) {
      await db.execute(
          `drop table ${tableName}`
      );
    }

    await db.execute(
        `create table ${tableName} (key_column int primary key, data_column text);`
    );
  }));

  await db.query(
      `select pg_create_logical_replication_slot('${slot}', 'wal2json')`
  );
}

export async function tearDownReplTables(slot, tables) {
  const slotCount = (await db.query(
    `select 1 from pg_replication_slots where slot_name = '${slot}'`
  )).rowCount;
  if (slotCount) {
    await db.query(
      `select pg_drop_replication_slot('${slot}')`
    );
  }

  await Promise.all(tables.map(async (tableName) => {
    const tableExists = await db.fetch(
      `select * from information_schema.tables where table_name = '${tableName}'`
    );
    if (tableExists) {
      await db.execute(
        `drop table ${tableName}`
      );
    }
  }));
}

beforeAll(async () => {
  await db.connect()
})

afterAll(async () => {
  const replicationSlot = new ReplicationSlot("data_distributor_slot_demo", config);
  const table = 'data_distributor_test_table';
  await tearDownReplTables(replicationSlot.getSlotName(), [table]);
  await db.close();
});

describe('test tracked table configuration', () => {
  test('reads configuration from file', () => {
    expect.assertions(1);
    const trackedTables = new ReplicationSlot("data_distributor_slot_demo", config);

    expect(trackedTables).toBeInstanceOf(ReplicationSlot);
  });

  test('returns list of tables as array of "schema.table" strings', () => {
    expect.assertions(5);
    const trackedTables = new ReplicationSlot("data_distributor_slot_demo", config);
    const tableList = trackedTables.getSchemaQualifiedTables();

    expect(tableList).toBeInstanceOf(Array);
    expect(tableList.length).toEqual(3);
    expect(tableList).toContain('public.data_distributor_test_table');
    expect(tableList).toContain('public.data_distributor_test_table_x');
    expect(tableList).toContain('public.data_distributor_test_table_y');
  });
});

describe('test database operations', () => {
  test("should fetch and clear changes", async() => {
    expect.assertions(5);

    const replicationSlot = new ReplicationSlot("data_distributor_slot_demo", config);
    const table = 'data_distributor_test_table';
    await setupReplTables(replicationSlot.getSlotName(), [table]);


    // no operations on tables so no changes
    const changes = await replicationSlot.fetch();
    expect(changes.length).toEqual(0);

    // do some inserts
    await db.execute(
        `insert into ${table} (key_column, data_column) values (1, 'a')`
    );
    await db.execute(
        `insert into ${table} (key_column, data_column) values (2, 'b')`
    );
    await db.execute(
        `insert into ${table} (key_column, data_column) values (3, 'c')`
    );
    await db.execute(
        `insert into ${table} (key_column, data_column) values (4, 'd'), (5, 'e')`
    );

    // four inserts produce 4 changes
    const originalResult = await replicationSlot.fetch();
    expect(originalResult.length).toEqual(4);


    // flush up to and including the second, item[1], change. only 2 items remain.
    const lsnToClear = originalResult[1].lsn;
    const lsnToRemain = originalResult[2].lsn;
    const lsnLast = originalResult[3].lsn;
    await replicationSlot.flush(lsnToClear);
    const newResult = await replicationSlot.fetch();
    expect(newResult.length).toEqual(2);

    // verify the remaining next lsn is the one that followed the flushed lsn.
    const lsnFinal = newResult[0].lsn;
    expect(lsnFinal).toEqual(lsnToRemain);

    await replicationSlot.flush(lsnLast);
    const finalResult = await replicationSlot.fetch();
    expect(finalResult.length).toEqual(0);
  });

  test("should only create one row when no slot tables edited", async() => {
    expect.assertions(2);

    const replicationSlot = new ReplicationSlot("data_distributor_slot_demo", config);
    const table = 'data_distributor_test_table';
    await setupReplTables(replicationSlot.getSlotName(), [table]);

    // no operations on tables so no changes
    const rows = await replicationSlot.fetch();
    expect(rows.length).toEqual(0);

    // do some inserts into a table we don't care about
    await db.execute(
      `create table if not exists data_dist_no_rep_slot_test_table ( key_column int, data_column varchar(5))`
    );
    await db.execute(
      `insert into data_dist_no_rep_slot_test_table (key_column, data_column) values (4, 'd'), (5, 'e')`
    );
    await db.execute(
      `insert into data_dist_no_rep_slot_test_table (key_column, data_column) values (1, 'a')`
    );

    // inserts into table not on replicationSlot should only produce 1 row.
    const originalResult = await replicationSlot.fetch();
    expect(originalResult.length).toEqual(1);
    await db.execute(`drop table data_dist_no_rep_slot_test_table`);
  });
  test("should not duplicate row if last edited was from table in slots", async() => {
    expect.assertions(2);

    const replicationSlot = new ReplicationSlot("data_distributor_slot_demo", config);
    const table = 'data_distributor_test_table';
    await setupReplTables(replicationSlot.getSlotName(), [table]);

    // no operations on tables so no changes
    const rows = await replicationSlot.fetch();
    expect(rows.length).toEqual(0);

    // do some inserts into a table we don't care about as well as one that we do care about
    await db.execute(
      `create table if not exists data_dist_no_rep_slot_test_table ( key_column integer, data_column varchar(5))`
    );
    await db.execute(
      `insert into data_dist_no_rep_slot_test_table (key_column, data_column) values (4, 'd'), (5, 'e')`
    );
    await db.execute(
      `insert into data_dist_no_rep_slot_test_table (key_column, data_column) values (1, 'a')`
    );
    await db.execute(
      `insert into ${table} (key_column, data_column) values (8, 'd')`
    );

    // inserts into table not on replicationSlot should only produce row if they happen after last rep.Slot update.
    const originalResult = await replicationSlot.fetch();
    expect(originalResult.length).toEqual(1);
    await db.execute(`drop table data_dist_no_rep_slot_test_table`);
  });
});


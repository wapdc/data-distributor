import {expect, test} from "@jest/globals"
const {processQueueItem} = await import("../../src/handler.js")

test('Test process changes lambda', () => {
  let event = {
    Records: [
      {
        body: "{\"events\":[]}",
      }
    ]
  };
  processQueueItem(event).then((result) => {
    expect(result.statusCode).toBe(200);
  });
});
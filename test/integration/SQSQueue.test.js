import SQSQueue from "../../src/SQSQueue";
import {expect, describe} from '@jest/globals';
import {S3Client, HeadObjectCommand, DeleteObjectCommand} from "@aws-sdk/client-s3";
import env from "dotenv"
env.config()

const event = {
  "job_name": "jurisdictions_by_election_year",
  "event": "replaceJurisdictionData",
  "kind": "insert",
  "schema": "public",
  "table": "candidacy",
  "data": {
    "election_code": "2018"
  }
}

const MAX_SQS_MESSAGE_SIZE = 256 * 1024;


describe('queue functions', () => {
  const sqsQueue = new SQSQueue(
    process.env.AWS_QUEUE_NAME
  );
  test('should send', async () => {
    const response = await sqsQueue.send({events: []});
    await expect(response['MessageId'].toString()).toMatch(
        /........-....-....-............/);
  });

  test('Send payloads over 256KiB to S3', async () => {
    expect.assertions(3)
    const message = { events: [...Array(2500)].map(() => event) }
    expect(Buffer.byteLength(JSON.stringify(message))).toBeGreaterThan(MAX_SQS_MESSAGE_SIZE)

    const putResponse = await sqsQueue.send(message)
    expect(putResponse.key.slice(-5)).toBe('.json')

    const s3Client = new S3Client()
    const params = {
      Bucket: process.env.LARGE_PAYLOAD_BUCKET,
      Key: putResponse.key
    }

    await s3Client.send(new DeleteObjectCommand(params));

    try {
      await s3Client.send(new HeadObjectCommand(params));
    } catch (err) {
      expect(err.$response.statusCode).toBe(404)
    }
  })
});

#!/usr/bin/env bash
set -e
export AWS_REGION="us-west-2"
export AWS_ACCESS_KEY_ID=$COMMERCIAL_AWS_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY=$COMMERCIAL_AWS_SECRET_ACCESS_KEY
export STAGE="demo"
export PGHOST="$RDS_WAPDC_DEMO_PGHOST"

# Install dependencies
echo "Installing dependencies"
npm install --quiet --no-progress

# Run integration tests
echo "Running tests against :$PGHOST"
npm run all-tests
echo "Complete"
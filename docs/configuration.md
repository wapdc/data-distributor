# Open Data Configuration

The configuration for each dataset and the mapping between the source table, source view, and source
column id to the target dataset and identifier column are defined in the `open_data_config` table.

```sql
select * from open_data_config
```

Here you will see the following a structure similar to the following example

| job_name | stage | config | is_enabled | last_run |
| ------ | ------ | ------ | ------ | ------ |
| fa_statement_update | demo | {} |  true |  |

Any additional table/dataset mapping must be defined here in order for the application to function properly.

## Inserting & Updating Configuration Items

A new configuration item can be inserted as follows:
```sql
insert into open_data_config (
        job_name,
        stage,
        config,
        is_enabled)
    values (
        'fa_statement_update',
        'demo',
        '{}',
        true
)
```

## Field Glossary

| Field | Description |
| ----- | ----- |
| **job_name** | An arbitrary identifier for the configuration. Should be unique. |
| **stage** | The stage of the deployment. Either demo or live. |
| **config** | JSON payload describing how the open data transform should work (see below).|
| **is_enabled** | Determines whether the configuration item is active or inactive. |

### Configuration.

Proposed structure for expressing configuration.

```JSON5
{
  "changes": [
    {
      "schema" : "public",
      "table": "candidacy",
      "changes": ["insert", "update"],
      "columns": ["fa_statement_id"],
      "event": "updateStatementFromCandidacy",
      "filter": {
        "fa_statement_id": '.+',
      }
    },
    {
      "schema" : "public",
      "table": "fa_statement",
      "changes": ["delete"],
      "columns": ["statement_id"],
      "event": "deleteStatement"
    },
    {
      "schema" : "public",
      "table": "fa_statement",
      "changes": ["delete"],
      "columns": ["statement_id"],
      "event": "replaceStatement"
    },
  ],
  "events": {
    "updateStatement": {
    },
    "updateStatementFromCandidacy": {
      "type": "upsert",
      "dataset_id": "fff123",
      "source": "select * from fa_open_data where id=$1",
      "criteria": ["fa_statement_id"]
    },
    "deleteStatement": {
      "type": "delete",
      "dataset_id": "ffff123",
      "source": "select id where id=$1",
      "criteria": ["fa_statement_id"]
    },
    "replaceStatement": {
      "type": "replace",
      "dataset_id": "ffff123",
      "source": "select id where id=$1",
      "criteria": ["fa_statement_id"],
    },
  }
}
```

## `config` Field Glossary

### `config.changes`

The changes section of the job is an array of the table changes that were listening too to fire an event.
Each entry in the changes array can have the following properties:

**schema** - The postgres schema that the table resides in.

**table** - The name of the table to listen for changes.

**changes** - An array of the different types of changes to listen for (insert, update or delete).

**columns** - An array of column names to pass along to the event. Only these columns get included in the event payload.

**event** - The name of the event to fire. The definitions of the event appear in the events section of the config.

**filter** - (Optional) An object whose properties define conditions that must be met in order for the event to fire.
The value of each property in the filter object is a regular expression. Each corresponding property in the table change
is tested against the regular expression. If all of the tests defined in the filter object passes only then is the
event fired.

### `config.events`

The events section of the config is an object whose properties define the open data operations that need to take place when an event is fired.
Each named event describes a single open data publishing event that may update multiple rows in the dataset.

Each property of the events object can have the following properties:

**type** - Indicates the type of open data operation to be performed. The current supported values are (upsert, replace and delete).

**dataset_id** - The id of the dataset on Socrata on which to perform the event.

**source** - This is the query to perform to determine which rows are to be affected. When the type is "upsert" this is the query to perform
against the database to determine which rows to upsert to Socrata. When the type is "delete" this is the SoQL that determines which records to remove from Socrata.

**destination** - When the type is "replace", this is the query used to obtain rows from Socrata when determining if there exist records that must be deleted.

**criteria** - Is an array containing the property names of the event payload that are to be used in the query in the order they appear in the query.
The first element in the array should correspond to $1 referenced in the query.

**id** - [Optional] Supplies an alternate Socrata PK that will be used during replace events to determine which records are affected.

### Related Revision Scripts and Locations

#### Core

- revisions/rev.2020-07-003.open-data-config.sh
  - Defines the initial schema of the open_data_config table
- revisions/rev.2020-08-003.alter_open_data_schema.sh
  - Alters it to contain JSON instead of certain columns that were present in the previous schema definition.

#### Financial Affairs

- db/postgres/revisions/rev.2020-07-001.opendata.sh
  - This is the view that the data_distributor uses to source the data for the records that it must either upsert or delete.
- db/postgres/revisions/rev.2020-08-001.demo_open_data_config.sh
  - This is the demo configuration item for financial affairs.



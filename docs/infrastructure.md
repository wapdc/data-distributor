# AWS Resource Configuration

A number of resources were necessarily provisioned outside the context of `template.yml` as part
of this project. It is important to know that, because of our use of the RDS Proxy to
handle connection pooling, the lambda functions themselves must reside inside the VPC. All
resources, if inside a VPC, reside in the sole default VPC.

## Network Resource Inventory
The current network configuration includes the following resources:

> 1. VPC (vpc-cb7df5ac)
> 2. Subnets:
>    1. subnet-0a183452 (Public RDS / assigned to NAT Gateway)
>    2. subnet-4ea86565 (Public RDS)
>    3. subnet-4ea86565 (Public RDS)
>    4. subnet-4ea86565 (Public RDS)
>    5. subnet-0d573dd79b4cab837 (Private Lambda)
>    6. subnet-0ef566f202b1ff2f7 (Private Lambda)
> 3. Security Group (sg-3a58d242)
> 4. Internet Gateway (igw-7caaf618)
> 5. NAT Gateway (nat-033f8387afe6b547d)
> 6. Elastic IP Addresses:
>    - eipalloc-0487f0e856ef3b4ca (NAT Gateway)
>    - eipalloc-0b3ec8f1f1758844f (RDS Default)
> 7. Route tables:
>    1. rtb-e9a2808e (Default RDS Subnet Group table)
>    2. rtb-0482c0802b9f0986f (Private Lambda Subnet table)

## Network Configuration
Certain resources exist as a result of RDS itself. When an RDS instance is provisioned, it
is automatically placed into a VPC with as many subnets as there are Availability Zones in that particular region. By default, it is not publicly accessible. However, if the setting
"Make publicly accessible" is selected, several additional network configuration changes are made.

- The Subnets in the VPC are associated with a Route Table allowing inbound access from 0.0.0.0/0.
- Each subnet receives a default route with a new Internet Gateway as its target.

This is the out-of-the-box network arrangement for a publicly accessible RDS instance.

#### RDS Proxy

The introduction of the RDS proxy requires changes to the Lambdas that restricts access to resources over HTTP.
The RDS Proxy and the Lambdas must be deployed into the same VPC as the RDS instance. This means that
communication between the lambda functions and, for example, Socrata or SQS via the SQS URL will not work. To correct this,
 we required a NAT Gateway, two additional private subnets, a new route table, and an elastic IPv4 address for the NAT resource.

The two private subnets are specifically for the lambdas. The `serverless.yml` VPC configuration should reflect only these:

```
  vpc:
    securityGroupIds:
      - sg-3a58d242
    subnetIds:
      - subnet-0d573dd79b4cab837
      - subnet-0ef566f202b1ff2f7
```

Each private subnet was associated with the new Route Table. The Route Table was configuration to have a default route for
`0.0.0.0/0` with the NAT Gateway (nat-033f8387afe6b547d) as its target.

The NAT Gateway itself was provisioned using the same default RDS VPC (the only VPC currently) and one of the four
public Subnets from the RDS subnet group. An elastic IP address was provisioned specifically for the NAT device.

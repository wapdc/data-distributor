import axios from 'axios';
import zlib from 'zlib'
import log from 'lambda-log'
import {getSecrets} from 'pdc-common/secrets-manager'

class Socrata {

  constructor() {
    this.axiosConfig = {};
  }

  async queryRecords(dsid, query = '', params = {}) {
    let url = this.baseUrl + dsid + '.json';
    let axiosConfig = {...this.axiosConfig}
    axiosConfig.params['$query'] = this.buildSoqlQuery(query, params);
    let response = await axios.get(url, axiosConfig).catch(error => {return error.response});
    return response.data;
  }

  async initialize() {
    let domain
    let auth
    if (process.env.STAGE === 'dev') {
      auth = {
        username: process.env.SOCRATA_USERNAME,
        password: process.env.SOCRATA_PASSWORD
      }
      domain = process.env.SOCRATA_DOMAIN
    }
    else {
      const socrata =  await getSecrets('Socrata')
      auth = {
        username: socrata.username,
        password: socrata.password,
      }
      domain = socrata.domain
    }
    this.axiosConfig = {
      auth,
      headers: { 'Content-Type': 'application/json', 'Content-Encoding': 'gzip' },
      params: {}
    };
    this.baseUrl = 'https://' + domain + '/resource/';
  }

  /**
   * Load socrata records based on an id.
   * @param dsid
   *   the data set id to update
   * @param query
   *   The solq query to execute
   * @param parameters
   *   The parameters to replace before execution.
   * @returns {Promise<void>}
   */
  async loadRecords(dsid, query, parameters) {
    let url = this.baseUrl + dsid + '.json';
    let axiosConfig = Object.assign({}, this.axiosConfig);
    axiosConfig.headers = { 'Accept-Encoding': 'gzip' }
    let replacedQuery = this.buildSoqlQuery(query, parameters)
    axiosConfig.params = { '$query': replacedQuery };
    let response = await axios.get(url, axiosConfig)
    return response.data;
  }

  async upsertRecords(dsid, rows) {
    let new_url = this.baseUrl + dsid + '.json';
    let axiosConfig = Object.assign({}, this.axiosConfig);

    const payload = await (() => {
      return new Promise((resolve, reject) => {
        zlib.gzip(JSON.stringify(rows), async (error, payload) => {
          if (error) reject(error);
          log.info(`Socrata payload for DSID: ${dsid}`, rows);
          resolve(payload);
        })
      })
    })()

    const response = await axios.post(new_url, payload, axiosConfig).catch(err => {
      log.info(`Socrata upsert error: `, { url: new_url, err });
      throw new Error( "Could not upsert data.");
    });
    return response.data;
  }

  buildSoqlQuery(query, params){
    const values = Object.values(params)
    values.forEach((value, i) => {
      // Replace apostrophes
      value = value.toString().replace(/'/g, "''")
      const key = `$${i+1}`;
      query = query.replace(key, `'${value}'`);
    })
    return query;
  }
}

export default Socrata;

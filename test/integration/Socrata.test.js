/**
 * @jest-environment node
 */
import Socrata from "../../src/Socrata";
import {describe, expect, test} from "@jest/globals";
import env from "dotenv"
env.config()
const opendata = new Socrata();
const dsid = 'yu2h-ujyu';

const freshRows = (rows) => {
  return JSON.parse(JSON.stringify(rows));
}


describe('open data functions', () => {
  const rows = [
    {
      id: "1",
      sample_text: "one"
    },
    {
      id: "2",
      sample_text: "two"
    }
  ];

  beforeEach(async () => {
    console.log('clearing dataset')
    await opendata.initialize();
    await clearDataset();
  })

  test('should crud data', async () => {
    let response;

    response = await opendata.upsertRecords(dsid, freshRows(rows));
    expect(response.Errors).toBe(0);
    expect(response.rows_created).toBe(2);

    response = await opendata.upsertRecords(dsid, rows);
    expect(response.Errors).toBe(0);
    expect(response.rows_updated).toBe(2);

    let params = [
      rows[0]['id'],
    ];
    let query = 'select * where id=$1';

    setTimeout(async () => {
      [response] = await opendata.queryRecords(dsid, query, params);
      expect(response.id).toBe(params[0]);
    }, 1000);
  }, 10000);
});

async function clearDataset() {
  const records = await opendata.queryRecords(dsid);
  const recordsToDelete = records.map(record => ({ id: record.id, ':deleted': true }));
  await opendata.upsertRecords(dsid, recordsToDelete);
}
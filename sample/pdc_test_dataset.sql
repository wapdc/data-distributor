-- Create a table that can be created
create table if not exists pdc_test_dataset
(
    id          serial not null primary key,
    sample_text text,
    sample_date date,
    sample_time timestamptz,
    sample_url  text,
    sample_json json
);
-- Create a view to represent the data in the table
create or replace view od_pdc_test_dataset as
    select id,
           sample_text,
           open_data_date_format(sample_date) as sample_date,
           sample_time,
           sample_url,
           cast(sample_json as text) as sample_json
      from pdc_test_dataset;

-- Create replication slot if it does not exist
select pg_create_logical_replication_slot('data_distributor_slot', 'wal2json') where not exists(
   select 1 from pg_replication_slots where slot_name = 'data_distributor_slot'
);

-- Insert configuration.
insert into open_data_config(
  job_name, stage, is_enabled, config
) values (
          'pdc_test_dataset',
          'demo',
          true,
          '{
  "changes": [
    {
      "schema": "public",
      "table": "pdc_test_dataset",
      "changes": [
        "insert",
        "update"
      ],
      "columns": [
        "id"
      ],
      "event": "updateTestData"
    },
    {
      "schema": "public",
      "table": "pdc_test_dataset",
      "changes": [
        "delete"
      ],
      "columns": [
        "id"
      ],
      "event": "deleteTestData"
    }
  ],
  "events": {
    "updateTestData": {
      "type": "upsert",
      "source": "select * from od_pdc_test_dataset where id=$1",
      "criteria": [
        "id"
      ],
      "dataset_id": "yu2h-ujyu"
    },
    "deleteReportData": {
      "type": "delete",
      "source": "select id where id=\"$1\"",
      "criteria": [
        "id"
      ],
      "dataset_id": "yu2h-ujyu"
    }
  }
}')
on conflict(job_name, stage)
do update set is_enabled = excluded.is_enabled, config=excluded.config;

-- Insert some sample data.
insert into pdc_test_dataset(sample_text, sample_date, sample_time, sample_url, sample_json)
  values ('Test Data', cast(now() as date), null, 'https://pdc.wa.gov', cast('{"test": "value"}' as json));

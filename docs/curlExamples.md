<h1>This document contains examples of interacting with the Socrata API via curl</h1>

Authentication is done with an API token. You can use your own or the Publish API token

**Upserting a csv file. The csv file must contain all the columns and a header row with all the correct, matching column names.**

```sh
curl -i -u 'your-api-token:your-api-secret' --header 'Content-Type: text/csv' \
--header "Transfer-Encoding: chunked" --data-binary @rows.csv 'https://data.wa.gov/resource/1234-abcd.json'
```

**Replacing all rows with a csv file. The csv file must contain all the columns and a header row with all the correct, matching column names.**

```sh
curl -i -u 'your-api-token:your-api-secret' -X PUT --header 'Content-Type: text/csv' \
--header "Transfer-Encoding: chunked" --data-binary @rows.csv 'https://data.wa.gov/resource/1234-abcd.json'
```

**Deleting a single row by ID. In the example, 1234-abcd is the dataset id and ca-2020-93800 is the value of the id column. The actual name of the column is irrelevant, it must be the column used as the unique identifier.**

```sh
curl -u 'your-api-token:your-api-secret' -X DELETE 'https://data.wa.gov/resource/1234-abcd/ca-2020-93800.json
```

**Deleting a large number of rows when you have the row IDs.**

Create a json file of the id values. The column identifier must be the name of the column that is the unique identifier. For example.

`[{"id" : 14350376,
":deleted" : true},
{"id" : 14464868,
":deleted" : true},
{"id" : 14514692,
":deleted" : true},
{"id" : 14619132,
":deleted" : true},
{"id" : 14619133,
":deleted" : true}]`

Then post the file.

```sh
curl -i -u 'your-api-token:your-api-secret' --header 'Content-Type: application/json' \
--header "Transfer-Encoding: chunked" --data-binary @rowsToDelete.json 'https://data.wa.gov/resource/1234-abcd.json'
```

**Creating a working copy of a dataset. This is useful for making changes and doing incremental appends on a large dataset before publishing it without impacting the production dataset.**

This example creates a working copy, truncates it, appends 2 files and then publishes it.

Run curl to create the working copy. If you get a 202 response, the request was queue's. Just wait a few seconds and try again until you get a 200 OK.

```sh
curl -s -D - -o createWorking.out -u 'your-api-token:your-api-secret' -X POST \
--header 'Content-Type: application/json' --data 'method=copy' \
'https://data.wa.gov/api/views/1234-abcd/publication.json?method=copy'
```

Then get the working copy dataset id from the response. This id is used in the rest of the process. Be careful to use the working copy ID for the rest.

```sh
grep -m 1 '"id" : "....-...."' createWorking.out
```

To truncate the dataset, you just do a HTTP PUT with an empty array as the data. Using PUT is a "replace" versus POST which does an append. Again, if you get a 202, you can just try again.

```sh
curl -s -D - -o truncate.out -u 'your-api-token:your-api-secret' -X PUT \
--header 'Content-Type: application/json' --data '[]' 'https://data.wa.gov/resource/wxyz-6789.json'
```

After the truncate is done, you can just start posting the data rows. This will append.

```sh
curl -i -u 'your-api-token:your-api-secret' --header 'Content-Type: text/csv' \
--header "Transfer-Encoding: chunked" --data-binary @rows-001.csv 'https://data.wa.gov/resource/wxyz-6789.json'
```

```sh
curl -i -u 'your-api-token:your-api-secret' --header 'Content-Type: text/csv' \
--header "Transfer-Encoding: chunked" --data-binary @rows.csv-002 'https://data.wa.gov/resource/wxyz-6789.json'
```

Finally, you can publish the dataset. Note the dataset id is still the id of the working copy. You need to provide it as the viewId and in the URL like below.

```sh
curl -s -D - -o publish.out -u 'your-api-token:your-api-secret' --header 'Content-Type: application/json' \
--data 'viewId=wxyz-6789&async=false' 'https://data.wa.gov/api/views/wxyz-6789/publication'
```

**Getting the csv from the dataset can be done efficiently with psql and it's faster than PhpStorm. It is csv compliant and handles quotes and escaping.**

```sh
# Get the prod password
. local/environment.sh

# Wherever you have a copy of the AWS certificate bundle. You can download it from AWS.
export CA_PEM_FILE="${HOME}/.aws/global-bundle.pem"
export CON_OPTIONS="?sslmode=verify-ca&sslrootcert=${CA_PEM_FILE}"

export CONNSTR="postgresql://wapdc:${RDS_WAPDC_PROD_PASSWORD}@${RDS_WAPDC_PROD_PGHOST}:5432/wapdc${CON_OPTIONS}"

# Creates a csv file names after the name of the view in the current directory
VIEW="od_loans"
psql "${CONNSTR}" -c "\copy (select * from ${VIEW}) to ~/temp/${VIEW}.csv with csv header"
```


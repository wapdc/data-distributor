import * as db from 'pdc-common/wapdc-db'

let openDataConfiguration;

class DBProcessor {
  async getConfiguration() {
    const configQueue = `select job_name, stage, is_enabled, CAST(config as JSON)
                         from open_data_config
                         where stage = (select value from wapdc_settings where property = 'stage')
                           and is_enabled = true`;

    if (!openDataConfiguration) {
      openDataConfiguration = await db.query(configQueue);
    }
    return openDataConfiguration;
  }

  async loadRecords(query, parameters) {
    return await db.query(query, parameters);
  }
}
const dbProcessor = new DBProcessor();
export default dbProcessor



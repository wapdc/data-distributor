# Use Cases 

This document describes typical publication events in terms of changes to the data
that necessitate publication.

## Examples of Near Real Time Updates

### Financial Affairs Filing

A user files a new financial affairs statement or a revision to an old one. Because we 
see a new or updated fa_statement record, we need to create a new entry int the Financial Affairs 
Disclosure dataset representing that financial affairs statement. 

### Report Filing

When a new entry in the report table is inserted, we need to potentially create new entries in the following data sets: 

- Campaign Finance Reporting History
- Contributions to Candidates and Poitical Committees
- Expenditures by Candidates 
- Loans to Candidates and Political Committees
- Pledges Reporting History
- Debt Reported by Candidates and Political Committees
- Candidate Surplus Funds Latest Reports (if the committee is surplus)
- Candidate Surplus Funds Reports

### Financial Report Amendment

When a report table entry is updated and it has a seperceded_id we need to remove all records containing the 
report id in the following data sets:  

- Campaign Finance Reporting History
- Contributions to Candidates and Poitical Committees
- Expenditures by Candidates 
- Loans to Candidates and Political Committees
- Pledges Reporting History
- Debt Reported by Candidates and Political Committees
- Candidate Surplus Funds Latest Reports (if the committee is surplus)
- Candidate Surplus Funds Reports

### Candidacy Update

When either a candidacy record or a person record is updated we may need to update name 
or candidacy status information in ALL data sets (both campaign finance and financial affairs)


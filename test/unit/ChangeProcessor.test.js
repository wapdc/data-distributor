import ChangeProcessor from '../../src/ChangeProcessor'
import dbProcessor from "../../src/DBProcessor.js"
import {jest} from '@jest/globals'
import {
  batchConfig,
  batchMessage,
  batchRequest,
  batchResponse,
  batchRDSResponse1,
  batchRDSResponse2
} from './batchData';

let financialAffairsStatement =
    [
      {
        "id": 61,
        "submission_id": null,
        "report_number": 100883131,
        "filer_id": "ARNEJ  230",
        "person_id": 25290,
        "name": "ARNETT JAIME",
        "receipt_date": "2019-02-04",
        "period_start": "2018-02-04",
        "period_end": "2019-02-03",
        "json": null,
        "certification_name": null,
        "certification_email": null,
        "url": "<a href=\"http://demo-wapdc.pantheonsite.io/browse/request-agency-records\">Request record</a>"
      }
    ];

let openDataStatement = [
  {
    "id": "61",
    "report_number": "100883131",
    "filer_id": "ARNEJ  230",
    "person_id": "25290",
    "name": "ARNETT JAIME",
    "receipt_date": "2019-02-04T00:00:00.000",
    "period_start": "2018-02-04T00:00:00.000",
    "period_end": "2019-02-03T00:00:00.000",
    "url": {
      "url": "http://demo-wapdc.pantheonsite.io/browse/request-agency-records",
      "description": "Request record"
    }
  }
];

const expendituresRDS = [
  {
    "id": 761520222,
    "report_number": "100490398",
    "origin": "A/LE50",
    "filer_id": "NASSC2 110",
    "amount": "11.00",
    "expenditure_data": "2012-07-31T00:00:00"
  },
  {
    "id": 761520223,
    "report_number": "100490398",
    "origin": "A/LE50",
    "filer_id": "NASSC2 110",
    "amount": "217.00",
    "expenditure_data": "2012-07-31T00:00:00"
  },
];

const expendituresSocrata = [
  {
    "id": "761520222",
    "report_number": "100490398",
    "origin": "A/LE50",
    "filer_id": "NASSC2 110",
    "amount": "0.00",
    "expenditure_data": "2012-07-31T00:00:00"
  },
  {
    "id": "761520224",
    "report_number": "100490398",
    "origin": "A/LE50",
    "filer_id": "NASSC2 110",
    "amount": "0.00",
    "expenditure_data": "2012-07-31T00:00:00"
  }
]

const replacementUpsert = [
  {
    "id": 761520222,
    "report_number": "100490398",
    "origin": "A/LE50",
    "filer_id": "NASSC2 110",
    "amount": "11.00",
    "expenditure_data": "2012-07-31T00:00:00"
  },
  {
    "id": 761520223,
    "report_number": "100490398",
    "origin": "A/LE50",
    "filer_id": "NASSC2 110",
    "amount": "217.00",
    "expenditure_data": "2012-07-31T00:00:00"
  },
  {
    "id": "761520224",
    ":deleted": true
  },
]

let insertMessage = {
      "MessageId": "864f09a0-603d-404f-bcb2-942b8674b2b2",
      "ReceiptHandle": "AQEBzUNDv9pcAG047Gc8zVII9X8d9/WkaQvLSQ3Hj6Uf6nmoOi7U50BO4ielu0OWt/wAuIaxt/v/9et6yA7Scrdf1xskXGhky6DUwchLr/FUKaoSM5V9jDUXfYYL90MGdDJVfKR6zFC41EzzbvE0h1aKSrqCib2D6zaaJSKCrtPXiE8dlmMi+jN8r+JXccSZKB5czu48ow/HLkCnHUKli6LH/Nu1V2GqHUq1DLXirbX2BocQ4VaFiZ+5YJb2hkNrVcaLjxZu6hFZlzm9UPe3KfzWkGwskBpiNms7ofgfkdGC/Ny3WAJuJAfFV5uxDvFHYgUJEarppbSZcuWNHiTw1YvlqlSUzD6TIA+Q7NcHvnBkfnSv6zqJP3iDqJ4hOoH0mFMIB5Teyzj8twHrAkbMju4K2A==",
      "MD5OfBody": "e520303d4199ebafe1df97c4cc716cba",
      "body": '{"lsn": "10E/74064D30","events": [{"job_name": "test_job", "event": "update", "data": {"statement_id": 61}}]}'
};

let deleteMessage = {
      "MessageId": "864f09a0-603d-404f-bcb2-942b8674b2b2",
      "ReceiptHandle": "AQEBzUNDv9pcAG047Gc8zVII9X8d9/WkaQvLSQ3Hj6Uf6nmoOi7U50BO4ielu0OWt/wAuIaxt/v/9et6yA7Scrdf1xskXGhky6DUwchLr/FUKaoSM5V9jDUXfYYL90MGdDJVfKR6zFC41EzzbvE0h1aKSrqCib2D6zaaJSKCrtPXiE8dlmMi+jN8r+JXccSZKB5czu48ow/HLkCnHUKli6LH/Nu1V2GqHUq1DLXirbX2BocQ4VaFiZ+5YJb2hkNrVcaLjxZu6hFZlzm9UPe3KfzWkGwskBpiNms7ofgfkdGC/Ny3WAJuJAfFV5uxDvFHYgUJEarppbSZcuWNHiTw1YvlqlSUzD6TIA+Q7NcHvnBkfnSv6zqJP3iDqJ4hOoH0mFMIB5Teyzj8twHrAkbMju4K2A==",
      "MD5OfBody": "e520303d4199ebafe1df97c4cc716cba",
      "body": '{"lsn": "10E/74064D30","events": [{"job_name": "test_job","event": "delete", "data": {"statement_id": 61}}]}'
    };

let replaceMessage = {
  "MessageId": "864f09a0-603d-404f-bcb2-942b8674b2b2",
  "ReceiptHandle": "AQEBzUNDv9pcAG047Gc8zVII9X8d9/WkaQvLSQ3Hj6Uf6nmoOi7U50BO4ielu0OWt/wAuIaxt/v/9et6yA7Scrdf1xskXGhky6DUwchLr/FUKaoSM5V9jDUXfYYL90MGdDJVfKR6zFC41EzzbvE0h1aKSrqCib2D6zaaJSKCrtPXiE8dlmMi+jN8r+JXccSZKB5czu48ow/HLkCnHUKli6LH/Nu1V2GqHUq1DLXirbX2BocQ4VaFiZ+5YJb2hkNrVcaLjxZu6hFZlzm9UPe3KfzWkGwskBpiNms7ofgfkdGC/Ny3WAJuJAfFV5uxDvFHYgUJEarppbSZcuWNHiTw1YvlqlSUzD6TIA+Q7NcHvnBkfnSv6zqJP3iDqJ4hOoH0mFMIB5Teyzj8twHrAkbMju4K2A==",
  "MD5OfBody": "e520303d4199ebafe1df97c4cc716cba",
  "body": '{"lsn": "10E/74064D39","events": [{"job_name": "test_job","event": "replace", "data": {"report_id": "100490398"}}]}'
};

let openDataSettings = [
  {
    job_name: 'test_job',
    config: {
      events: {
        "update": {
          "type": "upsert",
          "dataset_id": "fff123",
          "source": "select * from fa_open_data where id=$1",
          "criteria": ["statement_id"]
        },
        "delete": {
          "type": "delete",
          "dataset_id": "fff123",
          "source": "select id where id=$1",
          "criteria": ["statement_id"]
        },
        "replace": {
          "type": "replace",
          "dataset_id": "fff123",
          "source": "select * from od_expenditures where report_number=$1",
          "destination": "select id where report_number=$1",
          "criteria": ["report_id"]
        }
      }
    },
    is_enabled: true,
  }
];

let successfulUpsertResponse = {
  "rows_created": 1,
  "Errors": 0,
  "rows_updated": 0,
  "Rows Created": 1,
  "Rows Updated": 0,
  "rows_deleted": 0,
  "Rows Deleted": 0
};

let successfulRemoveResponse = {
  "rows_created": 0,
  "Errors": 0,
  "rows_updated": 0,
  "Rows Created": 0,
  "Rows Updated": 0,
  "rows_deleted": 1,
  "Rows Deleted": 1
};

let successfulReplaceResponse = {
  "rows_created": 1,
  "Errors": 0,
  "rows_updated": 1,
  "Rows Created": 1,
  "Rows Updated": 1,
  "rows_deleted": 1,
  "Rows Deleted": 1
};


describe('change processor functions', () => {
  let cp;
  let rds;
  const socrata = {};
  beforeEach(()=> {
    rds = dbProcessor;
    cp = new ChangeProcessor(rds, socrata);
    rds.getConfiguration = jest.fn().mockReturnValue(openDataSettings);
  });

  test('should upsert new record', () => {
    rds.loadRecords = jest.fn().mockResolvedValue(financialAffairsStatement);
    socrata.upsertRecords = jest.fn().mockResolvedValue(successfulUpsertResponse);
    return cp.processChange(insertMessage).then(() => {
      return cp.flushRows(() => {
        expect(rds.loadRecords).toHaveBeenCalledWith("select * from fa_open_data where id=$1", [61]);
        expect(cp.socrata.upsertRecords).toHaveBeenCalledWith('fff123', financialAffairsStatement);
      })
    });
  });

  test('should remove row from socrata', () => {
    socrata.loadRecords = jest.fn().mockResolvedValue(openDataStatement);
    socrata.upsertRecords = jest.fn().mockResolvedValue(successfulRemoveResponse);
    return cp.processChange(deleteMessage).then(() => {
      return cp.flushRows().then(() => {
        expect(cp.socrata.loadRecords).toHaveBeenCalledWith('fff123', 'select id where id=$1', [61]);
        expect(cp.socrata.upsertRecords).toHaveBeenCalledWith('fff123', openDataStatement);
      })
    })
  });

  test('should replace row from socrata', () => {
    socrata.loadRecords = jest.fn().mockResolvedValue(expendituresSocrata);
    rds.loadRecords = jest.fn().mockResolvedValue(expendituresRDS);
    socrata.upsertRecords = jest.fn().mockResolvedValue(successfulReplaceResponse);
    return cp.processChange(replaceMessage).then(() => {
      return cp.flushRows().then(() => {
        expect(cp.db.loadRecords).toHaveBeenCalledWith("select * from od_expenditures where report_number=$1", ["100490398"]);
        expect(cp.socrata.loadRecords).toHaveBeenCalledWith('fff123', 'select id where report_number=$1', ["100490398"]);
        expect(cp.socrata.upsertRecords).toHaveBeenCalledWith('fff123', replacementUpsert);
      })
    })
  });

  test('should error on RDS error', () => {
    expect.assertions(2);
    rds.loadRecords = jest.fn().mockImplementation(async () => {
      throw new Error(`Something`)
    });
    return cp.processChange(insertMessage)
      .then(() => cp.flushRows())
      .catch(error => {
        expect(error).toBeDefined();
        expect(error.message).toBe(`Something`);
    })
  });

  test('should error on Socrata error', () => {
    expect.assertions(2);
    rds.loadRecords = jest.fn().mockImplementation(async () => financialAffairsStatement);
    socrata.upsertRecords = jest.fn().mockImplementation(async () => {
      throw new Error(`Something`)
    });
    return cp.processChange(insertMessage)
      .then(() => cp.flushRows())
      .catch(error => {
        expect(error).toBeDefined()
        expect(error.message).toBe(`Something`);
      });
  });

  test('Should send requests in batches', () => {
    rds.getConfiguration = jest.fn().mockReturnValue(batchConfig);
    cp.socrata.loadRecords = jest.fn().mockResolvedValue(expendituresSocrata);
    rds.loadRecords = jest.fn()
      .mockResolvedValueOnce(batchRDSResponse1)
      .mockResolvedValueOnce(batchRDSResponse2);
    socrata.upsertRecords = jest.fn().mockResolvedValue(batchResponse);
    return cp.processChange(batchMessage).then(() => {
      cp.flushRows().then(() => {
        expect(cp.socrata.upsertRecords).toHaveBeenCalledTimes(1);
        expect(cp.socrata.upsertRecords).toHaveBeenCalledWith('dsid-1234', batchRequest);
      })
    });
  })

});


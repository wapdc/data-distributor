import {SendMessageCommand, SQSClient} from '@aws-sdk/client-sqs';
import {S3Client, PutObjectCommand} from '@aws-sdk/client-s3';
import { v4 } from 'uuid';
import log from 'lambda-log';

const sqsClient = new SQSClient();
const s3Client = new S3Client();
class SQSQueue {
  constructor(queueName) {
    const awsRegion = process.env.AWS_REGION
    const accountId = process.env.AWS_ACCOUNT_ID
    this.url = `https://sqs.${awsRegion}.amazonaws.com/${accountId}/${queueName}`;
  }

  async send(message) {
    const payload = JSON.stringify(message);
    const size = Buffer.from(payload).length;
    if (size < 262144) {
      const command = new SendMessageCommand({
        QueueUrl: this.url,
        MessageBody: payload
      })
      return await sqsClient.send(command);
    } else {
      const key = `${v4()}.json`
      const command = new PutObjectCommand({
        Body: payload,
        Bucket: process.env.LARGE_PAYLOAD_BUCKET,
        Key: key
      })
      const response = await s3Client.send(command);
      log.info(`Payload size limit exceeded. Item sent to s3 with key ${key}: `);
      return { key, response: response.$metadata }
    }
  }
}

export default SQSQueue;

<h1>Diagnostics and Logging</h1>
<h3>This document contains information related to how logging works, what is available and things to help diagnose issues.</h3>

<hr>

**Reading errors in the Lambda processChange log**

The Lambda "data-distributor-prod-processChange" logs to cloud watch. When there are errors, The stream of JSON data 
that was sent to Socrata is included in the log but at least in the current version v1.1.7, the data is a string of 
comma seperated decimal values that represent the bytes of the stream. For example, the sequence of decimal values 
(91, 123) is the characters "[{" that denote the beginning of the json. To make things more complicated, the stream is 
most likely but not guaranteed to be gzip compressed so you need to convert from a list of decimal strings to bytes and 
then run the bytes through gzip to get the text.

Given a log entry that looks something like this:

```text
80,
          243,
          126,
          37,
          ...
```

Copy and paste the decimal strings into a text file named `decimal.txt` then run the following against the text file to produce the JSON 
representation. This assumes gzip. If it produces garbage, try it without gzip.

Using gzip:
```shell
perl -p -e 's/[, ]//g' decimal.txt | perl -ne 'printf pack( "c", $_)' | gunzip > data.json
```

Without gzip:
```shell
perl -p -e 's/[, ]//g' decimal.txt | perl -ne 'printf pack( "c", $_)' > data.json
```

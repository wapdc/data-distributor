#!/usr/bin/env bash
export AWS_REGION="us-west-2"
export AWS_ACCESS_KEY_ID=$COMMERCIAL_AWS_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY=$COMMERCIAL_AWS_SECRET_ACCESS_KEY
export STAGE="demo"
set -e
if [[ "$CI_COMMIT_TAG" ]] ; then
  echo "Tag build detected..."
  echo "Tag: $CI_COMMIT_TAG"
  export STAGE="prod"
else
  echo "Branch Build Detected"
  echo "Branch: $CI_COMMIT_REF_NAME"
  export STAGE="demo"
fi

# Install if we haven't already in a prior stage.
if [ ! -d node_modules ]; then
  echo "Installing dependencies"
  npm install --quiet --no-progress
fi

# Build Package for deployment.
echo "Building AWS sam package"
sam build
echo "Deploying AWS sam application"
sam deploy --no-fail-on-empty-changeset --config-env "$STAGE"
